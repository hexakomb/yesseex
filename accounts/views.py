from django.shortcuts import render,redirect
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.forms import AuthenticationForm
from accounts.forms import RegistrationForm,AccountUpdate
# Create your views here.


def signup_view(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)

        if form.is_valid():

            user = form.save()
            login(request, user)
            return redirect('cms:index')
    else:
        form = RegistrationForm()
    return render(request, 'accounts/signup.html',{'form':form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)


        if form.is_valid():
            user =form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('cms:index')

    else:
        form = AuthenticationForm()
    return render(request,'accounts/login.html',{'form':form})


def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('accounts:login')




def account_view(request):
    if not request.user.is_authenticated:
        return redirect('login')
    context = {}
    if request.POST:
        form = AccountUpdate(request.POST,instance=request.user)
        if form.is_valid():
            form.save()
    else:
        form = AccountUpdate(
            initial = {
                "first_name": request.user.first_name,
                "last_name": request.user.last_name,
                "email": request.user.email,
                "username": request.user.username
            }
        )
    context['form']=form
    return render(request,'accounts/edit.html',context)