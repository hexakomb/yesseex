from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from accounts.models import Account

# Register your models here.


class AccountAdmin(UserAdmin):
    model = Account
    list_display = ('email', 'username', 'date_joined', 'is_admin', 'is_staff', 'first_name', 'last_name')

    search_fields = ('email','username')

    readonly_fields = ('date_joined','last_login')
    fieldsets = (
        # (None, {'fields': ('password',)}),
        ('Base info',{'fields':('email','username',)}),
        ('Personal info', {'fields': ('date_joined',)}),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    filter_horizontal = ()
    list_filter = (['date_joined','username'])


admin.site.register(Account, AccountAdmin)
