from django.db import models
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager
# Create your models here.


class AccountManager(BaseUserManager):
    # parameter are the field required in registration
    def create_user(self,first_name,last_name,email,username,password):
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('User must have a username')

        user = self.model(
            # normalize_email(Turn email character into lowercases)
            email=self.normalize_email(email),
            username=username,
            first_name=first_name,
            last_name=last_name
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,first_name,last_name,email,username,password):
        user = self.create_user(
            first_name=first_name,
            last_name=last_name,
            # normalize_email(Turn email character into lowercases)
            email=self.normalize_email(email),
            username=username,
            password=password
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    email = models.EmailField(verbose_name='email',max_length=60,unique=True)
    username = models.CharField(max_length=30,unique=True,blank=False,null=False)
    date_joined = models.DateTimeField(verbose_name='date joined',auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login',auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    first_name = models.CharField(verbose_name='first name',max_length=250,)
    last_name = models.CharField(verbose_name='last name',max_length=250)

    objects = AccountManager()
        # Setting what to use in login
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','last_name','username']

    def __str__(self):
        return self.email

    def has_perm(self,perm,obj=None):
        return self.is_admin

    def has_module_perms(self,app_label):
        return True
