from CMS.api.viewsets import LeadViewSet,ContactViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register('leads', LeadViewSet, basename='lead')
router.register('contacts',ContactViewSet, basename='contact')
#
# for url in router.urls:
#     print(url,"\n")