from django import forms
from CMS.models import *
from django.forms import Textarea, TextInput,FileField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout,Submit, Field


class LeadForm(forms.ModelForm):

    class Meta:
        model = Lead
        fields = ('name','email','address')

    def __init__(self,*args, **kwargs):
        super(LeadForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper

        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Field('name', css_class='form-control',),
            Field('email', css_class='form-control'),
            Field('address', css_class='form-control'),

            Submit('submit', 'Save', css_class='btn btn-success mt-2')
        )


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = ('name','email','phone','primary')

    def __init__(self,*args, **kwargs):
        super(ContactForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Field('name', css_class='form-control'),
            Field('email', css_class='form-control'),
            Field('phone', css_class='form-control'),
            Field('primary', css_class=' mt-2'),
            Submit('submit', 'Save', css_class='btn btn-success')
        )


class MeetingForm(forms.ModelForm):

    class Meta:
        model = Meeting
        fields = ('title','avenue','minute','about','conclusion','jsonTest')
        widgets = {
            'about': Textarea(attrs={'rows': 3}),
            'conclusion': Textarea(attrs={'rows': 3}),
        }
        labels = {
            'title': 'Meeting Title',
        }

    def __init__(self,*args, **kwargs):
        super(MeetingForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Field('title', css_class='form-control mb-3'),
            Field('avenue', css_class='form-control mb-3'),
            Field('minute',css_class="form-control mb-3"),
            Field('about', css_class='form-control mb-3'),
            Field('conclusion',css_class='form-control mb-3',row=3),
            Field('jsonTest',css_class='form-control mb-3',row=3),
            Submit('submit', 'Save', css_class='btn btn-success mt-3',)
        )


