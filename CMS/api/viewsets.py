from CMS.models import Lead,Stage,Contact
from .serializers import LeadSerializer,ContactSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import status
from django_filters import rest_framework as filters
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated



class LeadViewSet(viewsets.ModelViewSet):
    stage=''
    queryset = Lead.objects.all()
    serializer_class = LeadSerializer

    # authentication per view set basis
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    @action(methods=['get'], detail=True)
    def stage(self, request, pk='lead_id'):
        print(pk)
        lead = Lead.objects.get(pk=pk)
        if Stage[lead.stage].value != 4:
            self.stage = Stage(Stage[lead.stage].value + 1).name
        else:
            self.stage="none"
        serializer= LeadSerializer(lead,many=False)
        data = {"lead": serializer.data, "stage": self.stage}
        return Response(data)

    @action(methods=['get'], detail=False)
    def new(self,request):
        new = self.get_queryset().order_by('id').last()

        if Stage[new.stage].value != 4:
            self.stage = Stage(Stage[new.stage].value+1).name
        serializer = self.get_serializer_class()(new)
        data={"lead":serializer,"stage":self.stage}
        return Response(data)

    @action(detail=True, methods=['get'])
    def change_stage(self, request, pk='lead_id'):
        try:
            queryset = Lead.objects.get(pk=pk)
            # new = self.get_queryset().objects.filter(pk=pk)
            print(Stage[queryset.stage].value)

            if Stage[queryset.stage].value != 4:
                self.stage = Stage(Stage[queryset.stage].value + 1).name

            serializer = LeadSerializer(queryset, many=False)
            queryset.__setattr__('stage', self.stage)
            queryset.save()
            content = {"lead":serializer.data,"stage":self.stage,'code':200}
            return Response(content, status=status.HTTP_200_OK)
        except:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer


    @action(detail=True, methods=['get'])
    def bylead(self, request, pk='lead_id'):

        contacts= self.get_queryset().filter(lead__id=pk)
        serializer = ContactSerializer(contacts, many=True)
        content = {"contacts": serializer.data, 'code': 200}
        return Response(content, status=status.HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def regular(self, request,pk='contactId'):
        try:
            queryset = Contact.objects.get(pk=pk)
            queryset.__setattr__('primary', False)
            queryset.save()
            content = {'code': 200}
            return Response(content, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


    @action(detail=True, methods=['get'])
    def primary(self, request, pk='contactId'):
        try:
            queryset = Contact.objects.get(pk=pk)

            if Contact.objects.all().filter(primary=True).filter(lead__id= queryset.lead.id).exists():
                content = {'code': 300,'msg':'Customer can Have only one primary contact'}
                return Response(content, status=status.HTTP_200_OK)
            else:
                queryset.__setattr__('primary', True)
                queryset.save()
                content = {'code': 200}
                return Response(content, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)









