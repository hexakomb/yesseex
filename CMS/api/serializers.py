from rest_framework import serializers
from CMS.models import Lead, Contact


class LeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        # specify field to be serialized
        # fields = ('stage', 'name', 'address', 'email','id')

        fields = '__all__'




class ContactSerializer(serializers.ModelSerializer):
    lead = LeadSerializer(read_only=True)
    class Meta:
        model = Contact

        fields = '__all__'