from django.db import models
from datetime import date
from enum import Enum
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django_mysql.models import JSONField

# Create your models here.
private_storage = FileSystemStorage(location=settings.MEDIA_URL)
class Stage(Enum):
    LEAD = 1
    POTENTIAL = 2
    OPPORTUNITY = 3
    CUSTOMER = 4

class Lead(models.Model):
    stage = models.CharField(max_length=250,default=Stage.LEAD.name)
    name = models.CharField(max_length=250,unique=True)
    address = models.CharField(max_length=250, default=None,blank=True,null=True)
    email = models.EmailField(max_length=250, default=None)


    def __str__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length=250)
    primary = models.BooleanField(default=False)
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=250,default=None)

    def __str__(self):
        return self.name


class Meeting(models.Model):
    title = models.CharField(max_length=250)
    avenue = models.CharField(max_length=250)
    stage = models.CharField(max_length=12)
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE)
    about = models.TextField(default=None)
    conclusion = models.TextField(default=None)
    minute = models.FileField(default=None,storage=private_storage)
    jsonTest = JSONField()

    def __str__(self):
        return self.title


class Contract(models.Model):
    title = models.CharField(max_length=250)
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE)
    description = models.TextField(default=None)
    attachment = models.FileField(default=None)

    def __str__(self):
        return self.title


class Product(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField(default=None)

    def __str__(self):
        return self.name


class ContractProduct(models.Model):
    contract = models.ForeignKey(Contract, on_delete=models.PROTECT, related_name='contract')
    product = models.ForeignKey(Product, on_delete=models.PROTECT, related_name='product')
