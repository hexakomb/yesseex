
from CMS.form import ContactForm
from django.shortcuts import redirect, render
from django.views.generic import View
from CMS.models import Lead, Contact
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


@method_decorator(login_required, name='dispatch')
class ContactCreate(View):
    form_class = ContactForm
    template_name = 'cms/new_contact.html'

    def get(self,request,lead_id):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form,'lead_id':lead_id})

    def post(self, request,lead_id):
        form = self.form_class(request.POST)
        try:

            post_data = request.POST.dict()
            lead = Lead.objects.get(id=lead_id)

            if form.is_valid():
                if post_data.get('primary'):

                    if Contact.objects.all().filter(primary=True).filter(lead__id=lead_id).exists():
                        messages.error(request, 'Customer Can only have one primary contact')
                        return render(request, self.template_name, {'form': form, 'lead_id': lead_id})
                    else:
                        contact = form.save(commit=False)
                        contact.__setattr__('lead', lead)
                        contact.save()
                else:
                    contact = form.save(commit=False)
                    contact.__setattr__('lead', lead)
                    contact.save()
            return redirect('cms:lead-details', lead.id)
        except Exception as e:
            messages.error(request, 'Error Occurred try Again')
            return render(request, self.template_name, {'form': form, 'lead_id': lead_id})

