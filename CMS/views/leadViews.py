import time

from django.views import generic
from CMS.form import LeadForm
from django.shortcuts import render,redirect
from django.views.generic import View
from CMS.models import Lead, Stage
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages


@method_decorator(login_required, name='dispatch')
class IndexView(generic.ListView):
    template_name = 'cms/leads.html'
    context_object_name = 'leads'

    def get_queryset(self):
        return Lead.objects.all()


@method_decorator(login_required, name='dispatch')
class LeadView(generic.DetailView):
    stage = ''

    def get(self,request, pk='lead_id',**kwargs):
        lead = Lead.objects.get(pk=pk)
        if Stage[lead.stage].value != 4:
            self.stage = Stage(Stage[lead.stage].value+1).name
        return render(request, 'cms/lead_details.html', {'lead': lead, 'stage': self.stage})


@method_decorator(login_required, name='dispatch')
class LeadCreate(View):
    form_class = LeadForm
    template_name = 'cms/registration/newLead_form.html'
    details_template = 'cms/lead_details'

    def get(self,request):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form})

    def post(self, request):
        form = self.form_class(request.POST)
        try:


            if form.is_valid():
                lead = form.save(commit=False)
                lead.save()
                return redirect('cms:lead-details', lead.id)
            else:
                return render(request, self.template_name, {'form': form,})
        except Exception as e:
            messages.error(request, e)
            return render(request, self.template_name, {'form': form})



def job():
    print("hello")


