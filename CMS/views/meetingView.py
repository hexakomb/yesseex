from django_mysql.models.functions import RegexpReplace, JSONExtract

from CMS.form import MeetingForm
from django.shortcuts import redirect, render
from django.views.generic import View
from CMS.models import Lead,Meeting
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status


@method_decorator(login_required, name='dispatch')
class MeetingCreate(View):
    form_class = MeetingForm
    template_name = 'cms/new_meeting.html'

    def get(self,request,lead_id):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form,'lead_id':lead_id})

    def post(self, request,lead_id):
        form = self.form_class(request.POST, request.FILES)
        try:

            lead = Lead.objects.get(id=lead_id)
            if form.is_valid():
                meeting = form.save(commit=False)
                meeting.__setattr__('lead', lead)
                # meeting.attrs = {"people":[{"name":"ndayambaje","age":1},{"name":"john","age":1}]}
                meeting.save()
                return redirect('cms:lead-details', lead.id)
            return render(request, self.template_name, {'form': self.form_class, 'lead_id': lead_id})
        except:
            messages.error(request, 'Error occurred while saving new Meeting, try again later')
            return render(request,self.template_name,{'form':self.form_class,'lead_id':lead_id})

class MeetingUpdate(View):
    form_class = MeetingForm
    template_name = 'cms/new_meeting.html'

    def get(self, request, pk="pk", lead_id="lead_id"):
        meeting = Meeting.objects.get(pk=pk)
        form = self.form_class(instance=meeting)
        return render(request,self.template_name,{'form':form,'lead_id':lead_id})

    def post(self, request, pk="pk",lead_id="lead_id"):
        meeting = Meeting.objects.get(pk=pk)
        form = self.form_class(request.POST, request.FILES,instance=meeting)

        if form.is_valid():
            meeting = form.save(commit=False)
            meeting.jsonTest['name'] = "SAKIPU"
            meeting.save()
            print("test")
        return render(request, self.template_name, {'form': form, 'lead_id':lead_id})


@api_view(['POST', ])
def api_json_field_update(request, pk="pk"):
    meeting = Meeting.objects.get(pk=pk)
    meeting.jsonTest["people"] = request.data["people"]
    meeting.save()
    print(filter(lambda person: person['name'] == 'Malia', meeting.jsonTest["people"]))
    return Response(meeting.jsonTest, status.HTTP_200_OK)




class ViewMetting(View):

    def get(self,request):
        try:
            # meetings = Meeting.objects.filter(jsonTest__people__contains={"name":"Suleiman"})
            # meetings = Meeting.objects.filter(jsonTest__name="BAGABO")
            # meetings = Meeting.objects.filter(jsonTest__has_key="test")
            meetings = Meeting.objects.filter(jsonTest__has_any_keys=["people","test"])

            print(meetings)
            return render(request,"cms/registration/mettings.html",{'meetings':meetings})
        except:
            return render(request,"cms/registration/mettings.html")