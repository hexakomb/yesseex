from django.urls import include, path
from .views import leadViews,contactViews, meetingView

#below is the namespace of urls of mystatic app

app_name = 'cms'
urlpatterns = [

    # /index/
    # path('', album_view.IndexView.as_view(), name='index'),

    # /mystatic/<album_id>
    # path('<pk>', album_view.DetailView.as_view(), name='details'),
    path('lead/',leadViews.IndexView.as_view(),name='index'),
    path('lead/details/<pk>/', leadViews.LeadView.as_view(), name='lead-details'),
    path('lead/add/', leadViews.LeadCreate.as_view(), name='lead-add'),
    path('contact/add/<lead_id>',contactViews.ContactCreate.as_view(), name='contact_add'),
    path('meeting/add/<lead_id>',meetingView.MeetingCreate.as_view(), name='meeting_add'),
    path("meeting/view",meetingView.ViewMetting.as_view(),name="view_meeting"),
    path("meeting/update/<pk>/<lead_id>", meetingView.MeetingUpdate.as_view(), name="update_meeting"),
    path("api/meeting/update/<pk>",meetingView.api_json_field_update, name="update_jsonfield")

]