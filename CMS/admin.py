from django.contrib import admin
from CMS.models import Contact, Lead, Meeting, Contract, Product,ContractProduct


# Register your models here.

admin.site.register(Lead)
admin.site.register(Contact)
admin.site.register(Contract)
admin.site.register(ContractProduct)
admin.site.register(Product)
admin.site.register(Meeting)