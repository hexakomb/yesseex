import sched
import time

# s = sched.scheduler(time.time, time.sleep)
# def print_time(a='default'):
#     print("From print_time", time.time(), a)
#
# s.enter(10, 1, print_time)
# s.run()
def myfunc():
    print("hello")

from apscheduler.schedulers.background import BackgroundScheduler

scheduler = BackgroundScheduler()
job = scheduler.add_job(myfunc, 'interval', hours=24)
scheduler.start()
